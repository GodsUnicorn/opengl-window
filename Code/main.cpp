#include <Windows.h>
#include <GL/glu.h>
#include <GL/gl.h>

static bool isRunning = false;

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2i(0,  1);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex2i(-1, -1);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex2i(1, -1);
    glEnd();
    glFlush();
}

LRESULT CALLBACK WindowProc(HWND handle, UINT msg, WPARAM wParam, LPARAM lParam) {
    static LRESULT result = 0;
    static PAINTSTRUCT paint;

    switch(msg) {
    case WM_PAINT:
        Display();
        BeginPaint(handle, &paint);
        EndPaint(handle, &paint);
        break;

    case WM_SIZE:
        glViewport(0, 0, LOWORD(lParam), HIWORD(lParam));
        PostMessage(handle, WM_PAINT, 0, 0);
        break;

    case WM_CLOSE:
        isRunning = false;
        break;

    default:
        result = DefWindowProc(handle, msg, wParam, lParam);
        break;
    }

    return result;

}

int CALLBACK WinMain(HINSTANCE thisInstance, HINSTANCE prevInstance, LPSTR cmdPrompt, int code) {

    HWND window;
    WNDCLASS windowClass = {0};
    HDC deviceContext;
    HGLRC openGLContext;
    PIXELFORMATDESCRIPTOR pixelDescriptor;
    int pixelFormat = 0;

    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowProc;
    windowClass.hInstance = thisInstance;
    windowClass.lpszClassName = "OpenGLWindow";

    if(!RegisterClass(&windowClass)) {
        MessageBox(0, "Failed to register window class.", "OH NO", MB_OK);
        return 1;
    }

    window = CreateWindow(windowClass.lpszClassName, "OpenGL Win32", WS_VISIBLE | WS_OVERLAPPEDWINDOW, 0, 0, 800, 600, 0, 0, thisInstance, 0);

    if(!window) {
        MessageBox(0, "Failed to create window.", "OH NO", MB_OK);
        return 1;
    }

    deviceContext = GetDC(window);

    memset(&pixelDescriptor, 0, sizeof(PIXELFORMATDESCRIPTOR));
    pixelDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pixelDescriptor.nVersion = 1;
    pixelDescriptor.dwFlags = PFD_SUPPORT_OPENGL |PFD_DRAW_TO_WINDOW;
    pixelDescriptor.iPixelType = PFD_TYPE_RGBA;
    pixelDescriptor.cColorBits = 32;

    pixelFormat = ChoosePixelFormat(deviceContext, &pixelDescriptor);
    DescribePixelFormat(deviceContext, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), 0);
    SetPixelFormat(deviceContext, pixelFormat, &pixelDescriptor);

    openGLContext = wglCreateContext(deviceContext);
    wglMakeCurrent(deviceContext, openGLContext);

    if(window) {
        isRunning = true;
        while(isRunning) {
            MSG message;
            while(PeekMessage(&message, 0, 0, 0, PM_REMOVE)) {
                if(message.message == WM_QUIT)
                    isRunning = false;

                TranslateMessage(&message);
                DispatchMessage(&message);
            }

            // My code

            // End code
        }
    }

    wglMakeCurrent(0,0);
    ReleaseDC(window, deviceContext);
    wglDeleteContext(openGLContext);
    DestroyWindow(window);

    return 0;
}
